package utils

import (
	"errors"
	"gopkg.in/gomail.v2"
)

var (
	smtpHost = ""
	smtpPort = 0
	smtpUsername = ""
	smtpPassword = ""
)

func InitMail(host string, port int, username, password string) {
	smtpHost = host
	smtpPort = port
	smtpUsername = username
	smtpPassword = password
}

func SendMail(sender, recipient, subject, message string) *ErrorList {
	if smtpHost == "" {
		return CreateErrorList(errors.New("no smtp host configured"), "you have to supply a smtp host in the configuration", 910006404)
	}
	m := gomail.NewMessage()
	m.SetHeader("From", sender)
	m.SetHeader("To", recipient)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", message)

	d := gomail.NewDialer(smtpHost, smtpPort, smtpUsername, smtpPassword)

	if err := d.DialAndSend(m); err != nil {
		logger.Errorf("An error occurred while sending an email to \x1b[33m%s\x1b[0m !", recipient)
		return CreateErrorList(err, "could not send email", 910016500)
	}
	return nil
}