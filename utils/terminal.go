package utils

import (
	"bufio"
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"os"
	"strings"
	"syscall"
)

// let the user enter clear text
func GetText(prompt string) string {
	// print the message
	fmt.Print(prompt)
	// create a new reader on stdin
	reader := bufio.NewReader(os.Stdin)
	// read the text
	text, _ := reader.ReadString('\n')
	// return the text but trim excess spaces (newline)
	return strings.TrimSpace(text)
}

// let the user enter a secret text
func GetTextHidden(prompt string) string {
	// print the message
	fmt.Print(prompt)
	//read the secret text as bytes
	byteText, _ := terminal.ReadPassword(int(syscall.Stdin))
	// convert to bytes
	text := string(byteText)
	// print a extra new line because the input was hidden and so was the newline (return)
	fmt.Println()
	// return the text but trim extra spaces (newline)
	return strings.TrimSpace(text)
}
