package utils

import "strconv"

// get the last n digits from an int
func LastNDigits(num, n int) int {
	// convert the int to an string
	str := strconv.Itoa(num)
	// if the string is shorter than te number of digits
	if len(str) <= n {
		// the number of digit get reduced
		n = len(str) + 1
	}
	// convert the slice of the string back to an int
	digits, _ := strconv.Atoi(str[len(str)-n:])
	return digits
}
