package middleware

import (
	"bytes"
	"encoding/base64"
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/errors"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/app"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	SessionKey  = 	"session-key"
	Authenticated = "authenticated"
	AccessLevel = 	"access-level"
	UserID = 		"user-id"
)


var (
	// a map of supported authentication header
	supportedHeader = map[string]bool {
		"Basic":	true,
	}
)



/*
 * | AccessLevel | Routes
 * +-------------+-----------------
 * |           0 | GET 	  /
 * | 		   1 | GET 	  /id/{id}
 * | 		   2 | GET 	  /{category}/stats
 * |           3 | GET 	  /{category}/{key}/{value} ; GET /{category}
 * | 		   4 | POST   /{category}
 * | 		   5 | PUT    /{category}/{key}/{value}
 * | 		   6 | DELETE /{category}/{key}/{value}
 * |		   7 | allow user create
 * |		   8 | full access
 * |		   9 | root
 */

// procedure code : 500
func Login(ctx iris.Context) *utils.ErrorList {
	// get the authentication header
	auth := ctx.GetHeader("Authorization")
	// if the header is empty return an error
	if len(auth) == 0 {
		return utils.CreateErrorList(errors.New("no header"), "you have to supply a authentication header", 500006401)
	}
	// if the method is not separated from the hash by a space return an error
	index := strings.Index(auth, " ")
	if index < 1 {
		return utils.CreateErrorList(errors.New("not supported"), "Header must follow this format: 'Method Hash'.", 500016401)
	}
	//check if authentication method is supported
	if !supportedHeader[auth[:index]]  {
		return utils.CreateErrorList(errors.New("not supported"), "only basic authentication is supported", 500026401)
	}
	// decode the hash to username:password
	decodedSlice, err := base64.StdEncoding.DecodeString(auth[index + 1:])
	if err != nil {
		return utils.CreateErrorList(err, "error decoding header", 500033500)
	}
	// convert to string
	decoded := string(decodedSlice)
	// get the position of the :
	index = strings.Index(decoded, ":")
	if index < 1 {
		return utils.CreateErrorList(errors.New("not supported"), "Data must follow this format: 'username:password'.", 500046401)
	}
	// split into username and password
	username := decoded[:index]
	password := decoded[index + 1:]
	// get the user with username from database
	user, errs := database.GetUser(username)
	// if the user cant be found the login is invalid
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "wrong username or password", 500052401)) //double error code
		// return with error
		return errs
	}
	// check if the password hashes are the same
	if bytes.Equal(database.GeneratePasswordHash(password, user.PasswordSalt), user.PasswordHash) {
		// generate a session id for the login
		sessionKey := database.GenerateSessionKey(user)
		// create session object and fill it information
		userSession := database.UserSession{
			SessionKey: 	sessionKey,
			UserID: 		user.ID,
			LoginTime: 		time.Now(),
			LastSeenTime: 	time.Now(),
			AccessLevel:	user.AccessLevel,
		}
		// add the session object to the database
		errs := database.AddSession(&userSession)
		if errs != nil {
			return errs
		}
		ctx.SetCookie(newCookie(SessionKey, base64.StdEncoding.EncodeToString(sessionKey), true, true))
		// set to authenticated and set access level
		ctx.SetCookie(newCookie(Authenticated, "true", true, false))
		ctx.SetCookie(newCookie(AccessLevel, strconv.Itoa(user.AccessLevel), true, false))
		ctx.SetCookie(newCookie(UserID, strconv.Itoa(user.ID), true, false))
		return nil
	}
	return utils.CreateErrorList(errors.New("unauthorized"), "wrong username or password", 500052401) //double error code
}

// procedure code : 510
func Logout(ctx iris.Context) *utils.ErrorList {
	// check for access level 0
	errs := checkAccessLevel(ctx,0)
	if errs != nil {
		errs.Add(utils.Error(errors.New("internal error"), "can't log out. Either not logged in or not permitted.", 510003500))
		return errs
	}
	// delete the session set in session-key cookie
	sessionKey, err := ctx.Request().Cookie(SessionKey)
	if err != nil {
		errs = utils.CreateErrorList(errors.New("authentication error"), "can't find the '{{SessionKey}}' cookie.", 510012500)
		return errs
	}
	database.DeleteSession([]byte(sessionKey.Value))
	if errs != nil {
		return errs
	}
	// set authenticated to false
	ctx.SetCookie(newCookie(Authenticated, "false", true, false))
	ctx.RemoveCookie(SessionKey)
	ctx.RemoveCookie(AccessLevel)
	return nil
}

// procedure code : 520
func checkAccessLevel(ctx iris.Context, accessNeeded int) *utils.ErrorList {
	var (
		accessGiven int
		userID int
		errs *utils.ErrorList
	)
	// get token if it written in a http header called adapi-access-token
	if token := ctx.GetHeader("adapi-access-token") ; token != "" {
		// validate the token and if it is valid get the user id and access level
		accessGiven, userID, errs = database.ValidateAccessToken([]byte(token), "")
	} else {
		// if the header is not set look for a session id cookie
		// get the content of the cookie
		sessionCookie, err := ctx.Request().Cookie(SessionKey)
		if err != nil {
			// if it is empty return an error
			return utils.CreateErrorList(errors.New("Forbidden"), "you have to supply a session key or a access token", 520002401)
		} else {
			sessionKey, err := base64.StdEncoding.DecodeString(sessionCookie.Value)
			if err != nil {
				// if it is empty return an error
				return utils.CreateErrorList(errors.New("Forbidden"), "error decoding the session key", 520013500)
			}
			// if the cookie is set validate the session id
			accessGiven, userID, errs = database.ValidateSessionKey(sessionKey)
		}
	}
	// ValidateSessionKey and ValidateAccessToken return -1 if they are not valid
	if accessGiven == -1 {
		errs.Add(utils.Error(errors.New("Forbidden"), "access to this route is not permitted with user information given.", 520022401))
		return errs
	} else if accessGiven >= accessNeeded {
		// check if access level of the user is high enough
		// if it is save the values in the context for later use
		ctx.Values().Set("userID", userID)
		ctx.Values().Set("access-level", accessGiven)
		return nil
	}
	return utils.CreateErrorList(errors.New("Forbidden"), "access to this route is not permitted with the access level given: " + strconv.Itoa(accessGiven), 520032401)
}

// procedure code : 530
// this function returns a context handler which checks for the access level provided when calling the function
func NeedsAccessLevel(accessNeeded int) func(ctx iris.Context) {
	// return the function
	return func(ctx iris.Context) {
		// set default status to response error list
		response := utils.NewREL(&ctx, 200)
		// check if access is granted
		errs := checkAccessLevel(ctx, accessNeeded)
		// if there are errors stop the execution and set a http status
		if errs != nil {
			ctx.StopExecution()
			response.Status(errs)
			return
		}
		// if everything is fine go to the next handler
		ctx.Next()
	}
}

// procedure code : 540
// creates a new cookie with settings from config/config.yml
func newCookie(name, value string, expires, httpOnly bool) *http.Cookie {
	c := &http.Cookie{}
	// if the cookie should expire and the expiration is not set to 0 or end of session
	if expires && app.Settings.SessionTimeout > 0 {
		// set expiration date and maximum age
		c.Expires = time.Now().Add(app.Settings.SessionTimeout)
		c.MaxAge = int(app.Settings.SessionExpiration.Seconds())
	}
	c.Name = name
	c.Value = value
	c.Domain = app.Settings.CookieDomain
	c.Path = app.Settings.CookiePath
	c.HttpOnly = httpOnly
	return c
}