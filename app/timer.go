package app

import (
	"encoding/json"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"net/http"
	"time"
)

// used to pass multiple handler to start timer function
type TimerHandler func()

// type to store multiple entities of one category
type Category []database.Entity
// type to store a map of categories
type Categories map[string]Category

// a channel to send close to timer go routine
var quit = make(chan struct{})

// start timer with a slice of time handlers
func startTimer(handlers ...TimerHandler) {
	// create new timer with an interval defined in config file
	ticker := time.NewTicker(Settings.RemoteInterval)
	// create new go routine
	go func() {
		// repeat
		for {
			// do whatever gets true first
			select {
			// if timer is over iterate over handlers and execute
			case <- ticker.C:
				for _, handler := range handlers{
					handler()
				}
			// if close signal is send return from the go routine
			case <- quit:
				// stop the timer
				ticker.Stop()
				return
			}
		}
	}()
}

// function to stop the timer
func stopTimer() {
	// close the channel
	close(quit)
}

// wrapper function to call start timer with handler
func InitTimer() {
	// start timer
	startTimer(checkForOldSessions, fetchFromRemote)
}

// check if there are any expired session ids
func checkForOldSessions() {
	// get all session ids
	sessions, errs := database.GetSessions()
	// if there is an error get the codes, show them in the output and return form the function
	if errs != nil {
		codes, _ := errs.GetCodes()
		App.Logger().Infof("An error occurred resulting in the following error(s): \x1b[31m%d\x1b[0m!", codes)
		return
	}
	// iterate over all sessions
	for _, session := range *sessions {
		// if time difference is larger the specified in config file
		if time.Since(session.LoginTime) > Settings.SessionExpiration {
			// delete session and log in output
			database.DeleteSession(session.SessionKey)
			App.Logger().Infof("Removed session key %#X for being %s old.", session.SessionKey, time.Since(session.LoginTime).String())
		}
	}
}

// function used to fetch data from remote sources
func fetchFromRemote() {
	// iterate over sources defined in config file
	for i, source := range Settings.RemoteSources {
		App.Logger().Infof("Fetching data from remote source %s.", source.Url)
		// check if source url is empty; if it is warn the user and continue with next source
		if source.Url == "" {
			App.Logger().Warnf("The url for remote source %d is empty!", i)
			continue
		}
		// create a local function to be used to create a http request and get a response
		createRequest := func(url, username, password string) (*http.Response, error) {
			var (
				response *http.Response
				err error
			)
			// create new request with no body
			request, err := http.NewRequest("GET", url, nil)
			if err != nil {
				return nil, err
			}

			// set required headers
			request.Header.Set("Accept", "application/json")
			request.Header.Set("Content-Type", "application/json")

			// create a http clint
			cli := &http.Client{}
			// if username is specified use http basic authentication
			if username != "" {
				request.SetBasicAuth(username, password)
			}
			// send request
			response, err = cli.Do(request)
			return response, err
		}
		// get a response from the source's url
		response, err := createRequest(source.Url, source.Username, source.Password)
		// if there is an error warn the user and continue with the next andler
		if err != nil {
			App.Logger().Warnf("An Error occurred while fetching data from %s!: %s", source.Url, err.Error())
			continue
		}
		// if the response status code is not 200 OK warn the user and continue with the next handler
		if response.StatusCode != 200 {
			App.Logger().Warnf("Got [\x1b[31m%d\x1b[0m] from \x1b[33m%s\x1b[0m but expected [\x1b[32m200\x1b[0m]", response.StatusCode, source.Url)
			continue
		}
		// variable to store the categories
		var result Categories

		// decode the response body
		json.NewDecoder(response.Body).Decode(&result)
		// set to false if adding a entity returns an error
		successful := true
		// set to true if a category was empty
		empty := false
		// if no category set to empty
		if len(result) <= 0 {empty = true}
		Loop:
		// iterate over categories
		for category, list := range result {
			// if there are no entities set to empty
			if len(list) <= 0 {empty = true}
			// iterate ove entities
			for _, item := range list {
				App.Logger().Infof("Adding a entity in %s as user %d", category, source.UserId)
				// add entity to database
				errs := database.AddEntity(category, item, source.UserId)
				// if an error occurs write a warning to output and exit the loop
				if errs != nil {
					codes, _ := errs.GetCodes()
					App.Logger().Warnf("An error occurred resulting in the following error(s): \x1b[31m%d\x1b[0m!", codes)
					successful = false
					break Loop
				}
			}
		}
		// if the result was empty write warning to the output
		if empty {
			App.Logger().Infof("Got nothing new.")
		} else if successful && source.OnSuccess != "" {
			// if inserting the entities was successful and the on success url is configured for this remote source
			// create a confirming request
			_, err = createRequest(source.OnSuccess, source.Username, source.Password)
			App.Logger().Infof("Fetching data from remote source successful. Calling %s to confirm the transmission.", source.OnSuccess)
		}
		// close the body
		response.Body.Close()
	}
}