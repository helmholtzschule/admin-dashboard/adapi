package app

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path/filepath"
	"time"
)


// type to store the program configuration
type Configuration struct {
	// set to true if you want to run in debug mode (not implemented yet) (Default: false
	Debug bool							`yaml:"debug" json:"debug"`
	// set the log level (Default: "debug")
	LogLevel string						`yaml:"log-level" json:"log-level"`

	// set the host iris should start from (Default: "localhost")
	Host string							`yaml:"host" json:"host"`
	// the port iris should listen to (Default: 8080)
	Port int							`yaml:"port" json:"port"`
	// the path were all api calls go to (Default: /api)
	ApiRoot string						`yaml:"api-root" json:"api-root"`
	// the paths were all user (management) calls go to (Default: /user)
	UserRoot string						`yaml:"user-root" json:"user-root"`
	// the paths were all log calls go to (Default: /log)
	LogRoot string						`yaml:"log-root" json:"log-root"`
	// the paths were all mail calls go to (Default: /mail)
	MailRoot string						`yaml:"mail-root" json:"mail-root"`


	// the time it takes for a session id to become invalid (Default: 6h) (Format: 1ns, 1ms, 1s, 1m, 1h)
	SessionExpiration time.Duration		`yaml:"session-expiration" json:"session-expiration"`
	// the time it takes for the session id cookie to expire (Default: -1) (-1 means when the browser is closed)
	SessionTimeout time.Duration 		`yaml:"session-timeout" json:"session-timeout"`

	// the domain all cookies are created for (Default: "localhost")
	CookieDomain string					`yaml:"cookie-domain" json:"cookie-domain"`
	// the path all cookies are created for (Default: "/")
	CookiePath string					`yaml:"cookie-path" json:"cookie-path"`

	// how often to fetch data from remote sources and remove old session ids
	RemoteInterval time.Duration 		`yaml:"remote-interval" json:"remote-interval"`
	// a list of remote sources to look up
	RemoteSources []RemoteSource		`yaml:"remote-sources" json:"remote-sources"`

	// the authentication information to be used when sending emails
	SmtpHost string						`yaml:"smtp-host" json:"smtp-host"`
	SmtpPort int						`yaml:"smtp-port" json:"smtp-port"`
	SmtpUsername string					`yaml:"smtp-username" json:"smtp-username"`
	SmtpPassword string					`yaml:"smtp-password" json:"smtp-password"`
}

// type to represent one remote source
type RemoteSource struct {
	// the url to call when fetching data
	Url string						`yaml:"url" json:"url"`
	// username and password to use to authenticate with http basic when calling server (if empty no authentication is used)
	Username string					`yaml:"username" json:"username"`
	Password string					`yaml:"password" json:"password"`
	// when inserting data use this user id as new owner
	UserId int						`yaml:"user-id" json:"user-id"`
	// url to call on successful insertion (if empty no server will be called on success)
	OnSuccess string				`yaml:"on-success" json:"on-success"`
}

// load configuration from file
// filename: file to use as config
// return: configuration object
func LoadConfig(filename string) *Configuration {
	// load default configuration to use as basis
	c := DefaultConfiguration()

	// get absolute file path
	yamlAbsPath, err := filepath.Abs(filename)
	if err != nil {
		panic(err)
	}

	// read data from file
	data, err := ioutil.ReadFile(yamlAbsPath)
	if err != nil {
		panic(err)
	}

	// unmarshal yaml to configuration object
	if err := yaml.Unmarshal(data, c); err != nil {
		panic(err)
	}
	return c
}

// return default configuration
func DefaultConfiguration() *Configuration {
	return &Configuration{
		Debug: false,
		LogLevel: "debug",
		Host: "localhost",
		Port: 8080,
		ApiRoot: "/api",
		UserRoot: "/user",
		LogRoot: "/log",
		MailRoot: "/mail",
		SessionExpiration: time.Hour * 6,
		SessionTimeout: -1,
		CookieDomain: "localhost",
		CookiePath: "/",
		RemoteInterval: time.Minute * 30,
	}
}