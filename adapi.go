package main

import (
	"flag"
	"fmt"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/app"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/routes"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/tools"
	"os"
)


func main() {
	// the first argument is the path to the program
	args := os.Args[1:]

	// create a new flag set for each sub-command
	helpCommand := flag.NewFlagSet("help", flag.ExitOnError)
	setupCommand := flag.NewFlagSet("setup", flag.ExitOnError)
	resetCommand := flag.NewFlagSet("reset", flag.ExitOnError)
	recoverCommand := flag.NewFlagSet("reset", flag.ExitOnError)
	updateCommand := flag.NewFlagSet("update", flag.ExitOnError)

	// the global usage function
	flag.Usage = func() {
		fmt.Println("usage: adapi [command] [options]")
		fmt.Println("")
		fmt.Println("Admin Dashboard backend application for managing a network.")
		fmt.Println("")
		fmt.Println("Use 'adapi help <command>' for more information about a command.")
		fmt.Println("")
		fmt.Println("    Commands:")
		fmt.Println("")
		fmt.Println("        setup       add an administrator account called 'root' to the database")
		fmt.Println("")
		fmt.Println("        reset       reset all information and add an administrator account called 'root' to the database")
		fmt.Println("")
		fmt.Println("        recover     reset the login information of the user 'root'.")
		fmt.Println("")
		fmt.Println("        update      reload all schema files.")
		fmt.Println("")
		fmt.Println("If no sub-command is specified, the backend application will run.")
		fmt.Println("")
	}

	// if there was an argument passed
	if len(args) > 0 {
		// call the specific sub-command or print the global usage
		switch args[0] {
		case "setup":
			setupCommand.Parse(args[1:])
		case "reset":
			resetCommand.Parse(args[1:])
		case "recover":
			recoverCommand.Parse(args[1:])
		case "update":
			updateCommand.Parse(args[1:])
		case "help":
			helpCommand.Parse(args[1:])
		default:
			flag.Usage()
			os.Exit(1)
		}
	} else {
		// if no argument was passed start the program normally
		// register routes
		routes.RegisterRoutes()
		app.App.Logger().Info("registered routes")
		// run the app
		app.Run()
	}

	// if the help sub-command was called
	if helpCommand.Parsed() {
		// get the help command arguments
		helpArgs := helpCommand.Args()
		// set the usage for the help command
		helpCommand.Usage = func() {
			defaultUsage := func() {
				fmt.Println("usage: adapi help <command>")
				fmt.Println("")
				fmt.Println("    Commands:")
				fmt.Println("")
				fmt.Println("        setup       add an administrator account called 'root' to the database")
				fmt.Println("")
				fmt.Println("        reset       reset all information and add an administrator account called 'root' to the database")
				fmt.Println("")
				fmt.Println("        recover     reset the login information of the user 'root'.")
				fmt.Println("")
				fmt.Println("        update      reload all schema files.")
				fmt.Println("")
			}

			// if there was an argument passed to the sub-command help
			if len(helpArgs)  == 1 {
				// print the command specific help message
				switch helpArgs[0] {
				case "setup":
					fmt.Println("usage: adapi setup")
					fmt.Println("")
					fmt.Println("The setup sub-command will add an administrator account called 'root' to the database.")
					fmt.Println("")
				case "reset":
					fmt.Println("usage: adapi reset")
					fmt.Println("")
					fmt.Println("The reset sub-command will reset all information and add an administrator account called 'root' to the database.")
					fmt.Println("")
				case "recover":
					fmt.Println("usage: adapi recover")
					fmt.Println("")
					fmt.Println("The recover sub-command will reset the login information of the user 'root'.")
					fmt.Println("")
				case "update":
					fmt.Println("usage: adapi update")
					fmt.Println("")
					fmt.Println("The update sub-command will reload all schema files.")
					fmt.Println("")
				default:
					// defaults to defaultUsage of help sub-command
					defaultUsage()
				}
			} else if len(helpArgs) == 0 {
				// if no argument was passed print the defaultUsage of help sub-command
				defaultUsage()
			}
		}
		// exit after message printing
		// call help-command usage
		helpCommand.Usage()
		os.Exit(1)
	}

	if setupCommand.Parsed() {
		// if the setup sub-command was passed run the setup tool
		tools.RunSetup()
		os.Exit(1)
	}

	if resetCommand.Parsed() {
		// if the reset sub-command was passed run the reset tool
		tools.RunReset()
		os.Exit(1)
	}

	if recoverCommand.Parsed() {
		// if the recover sub-command was passed run the recover tool
		tools.RunRecover()
		os.Exit(1)
	}

	if updateCommand.Parsed() {
		// if the update sub-command was passed run the update tool
		tools.RunUpdate()
		os.Exit(1)
	}
}
