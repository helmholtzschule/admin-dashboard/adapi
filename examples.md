`POST host:port/api/badges`

```json
{
    "name": "important",
    "type": "issue",
    "description": "this is really important!",
    "color": "#ed1e0b",
    "importance": 10
}
```

`POST host:port/api/boards`

```json
{
    "title": "development",
    "description": "day to day development board",
    "lists": []
}
```
```json
{
    "title": "development",
    "description": "day to day development board",
    "lists": [{
        "title": "op",
        "open": true,
        "closed": false,
        "badges": ["BA2"],
        "assignees": [],
        "milestone": "",
        "group": "intern.future",
        "issues": ["IS12", "IS5"]
    }]
}
```

`POST host:port/api/inventory`

```json
{
    "name": "LAN 5m CAT6",
    "type": "cable"
}
```
```json
{
    "name": "LAN 5m CAT6",
    "type": "cable",
    "state": "new",
    "description": "blue",
    "extra": {
        "plug-type": "rj45"
    },
    "owner": "Zimmermann",
    "location": "R107",
    "badges": ["BA1", "BA3"]
    }
```

`POST host:port/api/issues`

```json
{
    "title": "1",
    "description": "2",
    "open": true,
    "working": true,
    "related-issues": ["IS2", "IS4"],
    "badges": ["BA5", "BA2"],
    "tag": "TA2",
    "milestone": "MS4",
    "time": {
        "spent": [{
            "time": 2235,
            "user": "US2",
            "timestamp": 5346467563
        }],
        "due": 1432523523,
        "opened": 25234634,
        "closed": 256735,
        "assigned": 235253246,
        "working": 3434634634,
        "reviewed": 23523532523

    },
    "weight": 23,
    "assignees": ["US1", "US2"],
    "comments": [{
        "user": "US2",
        "time": 235252523,
        "comment": "some idea here comes!"
    }]
}
```

`POST host:port/api/log`

```json
{
   "time": 5876203847,
   "type": "login",
   "severity": 0,
   "host": "R107-PC12",
   "extra": "moschcaule"
}
```

`POST host:port/api/milestones`

```json
{
    "title": "Schulanfang 19/20",
    "description": "anfang des schuljahres 2019/20",
    "open": true,
    "issues": ["IS1", "IS4"],
    "badges": ["BA3", "BA7"],
    "tag": "TA5",
    "time": {
        "due": 2636364,
        "opened": 24636346,
        "closed": 2537474574
    },
    "assignees": ["US2", "US2"],
    "comments": [{
        "user": "US23",
        "time": 346376456,
        "comment": "some other crazy comment here comes!"
    }]
}
```

`POST host:port/api/rooms`

```json
{
"building": "Haupthaus",
"floor": "1. OG",
"room-number": "R108",
"name": "Raum",
"access": ["XY3", "XY0"]
}
```

`POST host:port/api/stats`

```json
{
"name": "powerup",
"interval": 5,
"type": "user-computer",
"time":235363363,
"data": 232366,
"extra": ["R107-PC01", "R107-PC04", "R107-PC09"]
}
```

`POST host:port/api/tags`

```json
{
    "name": "Schulanfang 19",
    "description": "ehartsrj",
    "associated-issues": ["IS23", "IS21"],
    "associated-milestone": "MS1"
}
```

`POST host:port/api/tickets`

```json
{
    "username": "sefsfs",
    "email": "affefss",
    "name": "sgs",
    "ticket": {
        "headline": "adwawd",
        "description": "fsefsf",
        "room": "R107",
        "involved_items": "sfsffsgsg",
        "severity": 32
    },
    "time": 31254256345
}
```