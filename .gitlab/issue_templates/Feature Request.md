~"Feature Request"
> **Please read this!**  
>
> Please use a descriptive Title.  
> Before opening a new issue, make sure to search for [possible duplicates](https://gitlab.com/helmholtzschule/admin-dashboard/adapi/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Feature+Request)!

### Description 
> What is missing?

### Changes
> Provide use case examples for what the feature can be used for.  

### Implementation
> Do you already have an idea how the new feature could work?  

> ## need help writing Markdown?  
>
> [gitlab markdown reference](https://gitlab.com/help/user/markdown#code-and-syntax-highlighting)  
> [create link to line of code](https://help.github.com/articles/getting-permanent-links-to-files/)  
>
> {- don't forget to delete all lines preceded by a '>'. -}  

/label ~"Feature Request"