package routes

import (
	"errors"
	"github.com/kataras/iris"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/app"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/middleware"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"gopkg.in/mgo.v2/bson"
	"strconv"
)

// the struct of a user
type NewUser struct {
	Username		string 	 `json:"username"`
	Password		string 	 `json:"password"`
	Name 			string	 `json:"name"`
	AccessLevel* 	int	  	 `json:"access-level"`
	PrivateEmail	string 	 `json:"private-email"`
	PublicEmails	[]string `json:"public-emails"`
	IconLink		string 	 `json:"icon-link"`
	Badges			[]string `json:"badges"`
}

//register all routes related to user management
func RegisterUserManagement () {
	// create a new user
	app.User.Post("/create", middleware.NeedsAccessLevel(7), handleUserPost)
	// get user by username
	app.User.Get("/{username}", middleware.NeedsAccessLevel(1), handleUserGet)
	// update a user
	app.User.Put("/{username}", middleware.NeedsAccessLevel(1), handleUserPut)
	// delete a user
	app.User.Delete("/{username}", middleware.NeedsAccessLevel(1), handleUserDelete)
	// reset the password of a user
	app.User.Post("/{username}/reset", middleware.NeedsAccessLevel(1), handleUserReset)
	// disable a user
	app.User.Put("/{username}/disable", middleware.NeedsAccessLevel(8), handleUserDisable)
	// enable a user
	app.User.Put("/{username}/enable", middleware.NeedsAccessLevel(8), handleUserEnable)
	// generate a token for a user
	app.User.Post("/{username}/token", middleware.NeedsAccessLevel(1), handleUserToken)
	// get a list of all tokens of a user
	app.User.Get("/{username}/token", middleware.NeedsAccessLevel(1), handleUserGetToken)
	// delete a specific token
	app.User.Delete("/{username}/token", middleware.NeedsAccessLevel(1), handleUserDeleteToken)
	// update a specific token
	app.User.Put("/{username}/token", middleware.NeedsAccessLevel(1), handleUserUpdateToken)
}


// procedure code : 600
func handleUserPost(ctx iris.Context) {
	//TODO:DONE!: check if access is high enough
	//TODO:DONE! check if username exists already
	//TODO:DONE! check if all data was provided
	//TODO:DONE! check if data is valid
	//TODO:DONE! generate entry for authdb
	//TODO:DONE! insert into mongodb
	//TODO:DONE! edit schema
	//TODO:DONE! when insert fails remove from authdb
	// create response error list
	response := utils.NewREL(&ctx, 200)
	var newUser NewUser
	// read the new user from the request
	ctx.ReadJSON(&newUser)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the current user by id
	currentUser, errs := database.GetUserByID(id)
	if errs != nil {
		response.Status(errs)
		return
	}
	// check whether all required fields are given
	// also the access level of the new user cant be greater than the access level of the current user
	fields := ""
	if newUser.Username == "" { fields += "username " }
	if newUser.Password == "" { fields += "password " }
	if newUser.Name == "" { fields += "name " }
	if newUser.AccessLevel == nil || *newUser.AccessLevel > currentUser.AccessLevel { fields += "access-level " }
	if newUser.PrivateEmail == "" { fields += "private-email " }
	if newUser.PublicEmails == nil { newUser.PublicEmails = []string{} }
	if newUser.Badges == nil { newUser.Badges = []string{} }
	if fields != "" {
		response.Status(utils.CreateErrorList(errors.New("invalid request"), "These fields where empty or invalid: " + fields + ".", 600006422))
		return
	}
	// check if there is a user with the username already
	if _, errs := database.GetUser(newUser.Username); errs != nil {
		// generate salt for password
		salt := database.GeneratePasswordSalt()
		// create user object for the authentication database
		// generate a password hash from password and salt
		authUser := database.User{
			Username: newUser.Username,
			PasswordHash: database.GeneratePasswordHash(newUser.Password, salt),
			PasswordSalt: salt,
			IsDisabled: false,
			AccessLevel: *newUser.AccessLevel,
			PrivateEmail: newUser.PrivateEmail,
		}
		// try to add user to authentication database
		errs = database.AddUser(&authUser)
		if errs != nil {
			response.Status(errs)
		}
		// get the created user back from database
		addedUser, errs := database.GetUser(newUser.Username)
		if errs != nil {
			errs.Add(utils.Error(errors.New("internal error"), "added user but wasn't able to find afterwards", 600014500))
			response.Status(errs)
		}
		// add a user in the content database so everyone is able to see basic information
		// add the id of the auth user
		errs = database.AddEntity("users", database.Entity(bson.M{"username":addedUser.Username, "name":newUser.Name, "public-emails":newUser.PublicEmails, "access-level":addedUser.AccessLevel, "icon-link":newUser.IconLink, "badges": newUser.Badges}), addedUser.ID)
		if errs != nil {
			errs.Add(utils.Error(errors.New("internal server error"), "added user to authdb but add to mongodb failed", 600024500))
			// if there is an error remove the previously created user
			derrs := database.DeleteUser(addedUser.Username)
			if derrs != nil {
				errs.Add(utils.Error(errors.New("internal server error"), "removing from authdb failed", 600034500))
				response.Status(errs)
				return
			}
			response.Status(errs)
		}
	} else {
		response.Status(utils.CreateErrorList(errors.New("creation failed"), "username already exists", 600046400))
	}
}


// procedure code : 610
func handleUserGet(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user to get
	username := ctx.Params().Get("username")
	// get the user from the authentication database
	user, errs := database.GetUser(username)
	// if there is an error return it
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 610002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is not the same as the id of the logged in user return an error
	// don't return an error if the logged in user is root
	if user.ID != id && id != 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 610012401))
		return
	}
	ctx.JSON(&user)
}


// procedure code : 620
func handleUserPut(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	//TODO:DONE! check if data is valid
	//TODO:DONE! allow access level only to be downgraded except from root
	//TODO:DONE! access level of root can't be downgraded
	//TODO:DONE! update authdb if necessary
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user to update
	username := ctx.Params().Get("username")
	// get the user from the authentication database
	user, errs := database.GetUser(username)
	// if there is an error return it
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 620002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is not the same as the id of the logged in user return an error
	// don't return an error if the logged in user is root
	if user.ID != id && id != 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 620012401))
		return
	}
	var updUser NewUser
	// read the new user data from the request
	ctx.ReadJSON(&updUser)
	// get the current user from the authentication database
	authUser, errs := database.GetUser(username)
	if errs != nil {
		response.Status(errs)
		return
	}
	// get the current public user data from the content database
	entityUser, errs := database.GetFromNEntities("users", bson.M{"username": username}, 0, 1)
	if errs != nil {
		response.Status(errs)
		return
	}
	// check whether all required fields are given
	// also the access level of the new user cant be greater than the access level of the current user
	if updUser.Name != "" { (*entityUser)[0]["name"] = updUser.Name }
	if updUser.AccessLevel != nil && (*updUser.AccessLevel < user.AccessLevel || id == 0) && user.ID != 0 { (*entityUser)[0]["access-level"] = updUser.AccessLevel; authUser.AccessLevel = *updUser.AccessLevel}
	if updUser.PrivateEmail != "" { authUser.PrivateEmail = updUser.PrivateEmail }
	if updUser.PublicEmails != nil { (*entityUser)[0]["public-emails"] = updUser.PublicEmails }
	if updUser.Badges != nil { (*entityUser)[0]["badges"] = updUser.Badges }
	if updUser.IconLink != "" { (*entityUser)[0]["icon-link"] = updUser.IconLink }
	// update the user in the authentication database
	errs = database.UpdateUser(username, authUser)
	if errs != nil {
		response.Status(errs)
		return
	}
	// update the user in the content database
	errs = database.UpdateEntity("users", bson.M{"username": username}, (*entityUser)[0])
	if errs != nil {
		response.Status(errs)
		return
	}
}


// procedure code : 630
func handleUserDelete(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	//TODO:DONE! check if user to delete is NOT root
	//TODO:DONE! reassign all "_owner" to root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user to delete
	username := ctx.Params().Get("username")
	// get the user from the username
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 630002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is not the same as the id of the logged in user return an error
	// don't return an error if the logged in user is root
	if user.ID != id && id != 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 630012401))
		return
	}
	// make sure it is not possible to delete root
	if id == 0 {
		response.Status(utils.CreateErrorList(errors.New("not allowed"), "can't delete root", 630025403))
	}
	// delete the user from the authentication database
	errs = database.DeleteUser(username)
	if errs != nil {
		response.Status(errs)
		return
	}
	// delete the user from the content database
	errs = database.DeleteEntity("users", bson.M{"username": username})
	if errs != nil {
		response.Status(errs)
		return
	}
	// reassign the owner of all entities owned by the deleted user to root
	// iterate trough all categories
	for index := range database.CATEGORIES {
		// get all entities of the category
		entities, errs := database.GetEntities(index, bson.M{})
		if errs != nil {
			response.Status(errs)
			return
		}
		// iterate over all entities
		for i := 0; i < len(*entities); i++ {
			// if the owner of the entity is the deleted user reassign it
			if (*entities)[i]["_owner"] == user.ID {
				(*entities)[i]["_owner"] = 0
				// update the entity with the new owner
				errs = database.UpdateEntity(index, bson.M{"_id": (*entities)[i]["_id"]}, (*entities)[i])
				if errs != nil {
					response.Status(errs)
					return
				}
			}
		}
	}
}


// procedure code : 621
func handleUserReset(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user who is about to get a new password
	username := ctx.Params().Get("username")
	// get the user from the authentication database
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 621002401))
		response.Status(errs)
		return
	}
	// if the logged in user is not root return an error
	// make sure the user about to get the password reset is not root
	if id != 0 || user.ID == 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 621012401))
		return
	}
	// make a map the data of the request is the assigned to
	data := make(map[string]string, 1)
	ctx.ReadJSON(&data)
	// if the new password field is empty return a error
	if data["password"] == "" {
		response.Status(utils.CreateErrorList(errors.New("missing information"), "password is not specified", 621026400))
	}
	// generate new password hash with old password salt
	user.PasswordHash = database.GeneratePasswordHash(data["password"], user.PasswordSalt)
	// update the user in the authentication database
	errs = database.UpdateUser(username, user)
	if errs != nil {
		errs.Add(utils.Error(errors.New("internal server error"), "failed to set new password", 621033500))
		response.Status(errs)
	}
}


// procedure code : 622
func handleUserDisable(ctx iris.Context) {
	//TODO:DONE! check if user is not owner and not root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user who is about to get disabled
	username := ctx.Params().Get("username")
	//get the user with username from the authentication database
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid or trying to use root", 622002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is the same as the id of the logged in user return an error
	// also return an error when trying to disable root
	if user.ID == id || user.ID == 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid or trying to disable root", 622012401))
		return
	}
	// set the user disabled
	user.IsDisabled = true
	// update the user in the authentication database
	errs = database.UpdateUser(username, user)
	if errs != nil {
		errs.Add(utils.Error(errors.New("internal server error"), "failed to disable user", 622023500))
		response.Status(errs)
	}
}


// procedure code : 623
func handleUserEnable(ctx iris.Context) {
	//TODO:DONE! check if user is not owner and not root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user who is about to be enabled
	username := ctx.Params().Get("username")
	// get the user with username from authentication database
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid or trying to use root", 623002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is the same as the id of the logged in user return an error
	// also return an error when trying to enable
	if user.ID == id || user.ID == 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid or trying to enable root", 623012401))
		return
	}
	// set the user enabled
	user.IsDisabled = false
	// update the user in the authentication database
	errs = database.UpdateUser(username, user)
	if errs != nil {
		errs.Add(utils.Error(errors.New("internal server error"), "failed to enable user", 623023500))
		response.Status(errs)
	}
}


// procedure code : 640
func handleUserToken(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the currently logged in user
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user who requested a access token
	username := ctx.Params().Get("username")
	// get the user from the authentication database
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 640002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is not the same as the id of the logged in user return an error
	// don't return an error if the logged in user is root
	if user.ID != id && id != 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 640012401))
		return
	}
	// read data from the request
	// data should contain the category th access token is valid for
	//if there is no category specified or it is empty the access token is valid for all categories
	category := make(map[string]string, 1)
	ctx.ReadJSON(&category)
	/*if category["category"] == "" {
		ctx.JSON(utils.CreateErrorList(errors.New("missing information"), "category is not specified", 97).JSON())
	}*/
	// create the new access token and fill it with information
	// generate a token
	token := database.AccessToken{
		UserID: user.ID,
		Token: database.GenerateAccessToken(),
		AccessLevel: user.AccessLevel,
		Category: category["category"],
	}
	// add the access token to the authentication database
	errs = database.AddToken(&token)
	if errs != nil {
		response.Status(errs)
	}
	// return tha access token
	ctx.JSON(&token)
}


// procedure code : 650
func handleUserGetToken(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the user currently logged in
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user who's tokens got requested
	username := ctx.Params().Get("username")
	// get the user from the authentication database
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 650002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is not the same as the id of the logged in user return an error
	// don't return an error if the logged in user is root
	if user.ID != id && id != 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 650012401))
		return
	}
	// get all tokens from the authentication database where the user id matches the id of the logged in user
	tokens, errs := database.GetTokensByUserId(user.ID)
	if errs != nil {
		response.Status(errs)
		return
	}
	// return all tokens found
	ctx.JSON(tokens)
}


// procedure code : 660
func handleUserDeleteToken(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the user currently logged in
	id, _ := ctx.Values().GetInt("userID")
	// get the username of the user from whom a token is about to get deleted
	username := ctx.Params().Get("username")
	// get user with username from authentication database
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 660002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is not the same as the id of the logged in user return an error
	// don't return an error if the logged in user is root
	if user.ID != id && id != 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 660012401))
		return
	}
	// read data from request
	// the data should contain the access token to be deleted
	data := map[string]string{}
	ctx.ReadJSON(&data)
	// get the token from data
	token := data["access-token"]
	// make sure the token is not empty
	if token == "" {
		response.Status(utils.CreateErrorList(errors.New("invalid input"), "access-token not found", 660026400))
		return
	}
	// delete the token from the authentication database
	errs = database.DeleteToken([]byte(token))
	if errs != nil {
		response.Status(errs)
		return
	}
}


// procedure code : 670
func handleUserUpdateToken(ctx iris.Context) {
	//TODO:DONE! check if user is owner or root
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// get the id of the user currently logged in
	id, _ := ctx.Values().GetInt("userID")
	// get the access level of the user currently logged in
	accessLevel, _ := ctx.Values().GetInt("access-level")
	// get the username who's access token should get updated
	username := ctx.Params().Get("username")
	// get the user with username from database
	user, errs := database.GetUser(username)
	if errs != nil {
		errs.Add(utils.Error(errors.New("unauthorized"), "username or authentication is invalid", 670002401))
		response.Status(errs)
		return
	}
	// if the id of the user from the database is not the same as the id of the logged in user return an error
	// don't return an error if the logged in user is root
	if user.ID != id && id != 0 {
		response.Status(utils.CreateErrorList(errors.New("unauthorized"), "username or authentication is invalid", 670012401))
		return
	}
	// get data from request
	// the data should contain the access token which should get updated and the new access level of the token
	data := map[string]string{}
	ctx.ReadJSON(&data)
	// get the token from data
	token := data["access-token"]
	// make sre the token is not empty
	if token == "" {
		response.Status(utils.CreateErrorList(errors.New("invalid input"), "access-token not found", 670026400))
		return
	}
	// get the access token from database
	accessToken, errs := database.GetToken([]byte(token))
	if errs != nil {
		response.Status(errs)
		return
	}
	var newAccessLevel int
	// make sure the access level is not empty
	fields := ""
	if data["access-level"] == "" {
		fields += "access-level "
	} else {
		// the access level has to be lower or equal to the access level of the logged in user
		newAccessLevel, _ = strconv.Atoi(data["access-level"])
		if newAccessLevel > accessLevel { fields += "access-level "	}
	}
	// if there were any errors report them back at once
	if fields != "" {
		response.Status(utils.CreateErrorList(errors.New("invalid request"), "These fields where empty or invalid: " + fields + ".", 670036422))
		return
	}
	// set the access level to the token
	accessToken.AccessLevel = newAccessLevel
	// set the category of the token
	accessToken.Category = data["category"]
	// update the token in the authentication database
	errs = database.UpdateToken([]byte(token), accessToken)
	if errs != nil {
		response.Status(errs)
		return
	}
}