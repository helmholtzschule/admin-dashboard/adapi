package routes

import (
	"errors"
	"github.com/kataras/iris"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/app"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/middleware"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
)


//register all routes related to user management
func RegisterLogs () {
	app.Log.Post("/add", middleware.NeedsAccessLevel(4), handlePostLog)

	app.Log.Post("/get", middleware.NeedsAccessLevel(3), handleGetLog)
}


// POST
// description: add new log entry to database
// path: /add
// procedure code : 700
func handlePostLog(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 201)
	var entry database.LogEntry
	err := ctx.ReadJSON(&entry)
	if err != nil {
		response.Status(utils.CreateErrorList(err, "can't get data from request", 700002401))
		return
	}
	// check whether all required fields are given
	fields := ""
	if entry.Time == 0 { fields += "time " }
	if entry.LogType == "" { fields += "log-type " }
	if entry.IP == "" { fields += "ip " }
	if fields != "" {
		response.Status(utils.CreateErrorList(errors.New("invalid request"), "These fields where empty or invalid: " + fields + ".", 700016422))
		return
	}
	errs := database.AddLog(entry)
	if errs != nil {
		response.Status(utils.CreateErrorList(err, "unable to add log entry", 700022401))
		return
	}
}

// GET
// description: get log entries from database
// path: /get
// procedure code : 701
func handleGetLog(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 201)
	var data adapiData
	err := ctx.ReadJSON(&data)
	if err != nil {
		response.Status(utils.CreateErrorList(err, "can't get data from request", 701002401))
		return
	}
	if data.Count == 0 {
		data.Count = 32
	}
	logType := ""
	host := ""
	if data.Find != nil {
		if data.Find["log-type"] != nil {logType = data.Find["log-type"].(string)}
		if data.Find["host"] != nil {host = data.Find["host"].(string)}
	}
	entries, errs := database.GetLog(logType, host, data.From, data.Count)
		if errs != nil {
		response.Status(errs)
		return
	}
	response.Write(entries)
}