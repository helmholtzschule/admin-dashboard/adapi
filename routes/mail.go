package routes

import (
	"github.com/kataras/iris"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/app"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/middleware"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
)

func RegisterMail() {
	app.Mail.Post("/send", middleware.NeedsAccessLevel(7), sendMail)
}

func sendMail(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 200)
	var data struct{
		Sender string
		Recipient string
		Subject string
		Message string
	}
	ctx.ReadJSON(&data)
	errs := utils.SendMail(data.Sender, data.Recipient, data.Subject, data.Message)
	if errs != nil {
		response.Status(errs)
	}
}
