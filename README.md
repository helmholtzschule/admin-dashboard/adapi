# Admin Dashboard - API

[![pipeline badge](https://gitlab.com/helmholtzschule/admin-dashboard/adapi/badges/master/pipeline.svg)](https://gitlab.com/helmholtzschule/admin-dashboard/adapi/commits/master)

## installation

1. Download adapi.
2. Install mongodb.
3. Install mysql or mariadb.
4. Run the setup.sh script.
    - If the script fails you need setup the databases by yourself.
    - Mongo needs to have a user called adapi with the password ipada.
    - The mysql or maria database needs to have a database which is set up like in setup.sh.
    - Also there has to be the user adapi with the password ipada.
    - On both databases the users have to have read write access to the database.
5. Run ```./adapi setup``` in the directory where adapi is saved.
    - You are going to need to insert a email address and a password or the new root user.
    - The root user is the user who can't get deleted and who has the right to do anything.

## Usage

### authentication

There are two was of authenticating when sending requests to the api. One is to login and get a session id. The other is to add a access token to a user.

The login route is:
```POST ip:port/login```
with a basic http authentication header with username and password.  
The logout route is:
```GET ip:port/logout```

After login adapi sets the ```session-key``` cookie.

### access levels
Every user has a specific access level

| AccessLevel | description | Routes
|------------:|-------------|:----------------
|           0 | get general server information | GET 	  /
| 		    1 | get a entity by its id | GET 	  /id/{id}
| 		    2 | get stats of a category | GET 	  /{category}/stats
|           3 | search for entities and list all from one category | GET /{category}/{key}/{value}  GET /{category}
| 		    4 | add a new entity | POST   /{category}
| 		    5 | update a entity | PUT    /{category}/{key}/{value}
| 		    6 | delete a entity | DELETE /{category}/{key}/{value}
|		    7 | create a new user | POST /user/create
|		    8 | full access  | all
|		    9 | full access and the ability to access entities he does not own | root

A user has also the rights of the access levels below.

### user management
The data of a user can only be viewed if the user logged in or authenticated via access token. An exception is the root user. He can view and edit the data of every user.

---
#### create new user
Required access level: 7  
##### Request
Method: ```POST```  
URL: ```ip:port/user/create```  
Body (example):  
```json
{
  "username": "admin",
  "password": "supersavepassword",
  "name": "Admin Dashboard Admin",
  "access-level": 8,
  "private-email": "admin@institute.org",
  "public-emails": ["help@institute.org"],
  "icon-link": "http://institute.org/images/avatar.png"
}
```

##### Response
Status Code on success: ```200 OK```  

---
#### get user
Required access level: 1  
##### Request
Method: ```GET```  
URL: ```ip:port/user/{username}```  
Example: ```ip:port/user/admin```

##### Response
Status Code on success: ```200 OK```  
Body (example):  
```json
{
  "ID": 1,
  "Username": "admin",
  "PasswordHash": "ofTGzATtDKNfoCCgk+ESR+AGWJpdljIvFJMzRn0CBAs=",
  "PasswordSalt": "TKsgC0wT",
  "IsDisabled": true,
  "AccessLevel": 8,
  "PrivateEmail": "admin@institute.org"
}
```

---
#### update user
Required access level: 1  
Every property can be omitted when updating. If not specified the property will not be changed.
##### Request
Method: ```PUT```  
URL: ```ip:port/user/{username}```  
Example: ```ip:port/user/admin```  
Body (example):  
```json
{
  "name": "Admin Team",
  "access-level": 8,
  "private-email": "admin@institute.org",
  "public-emails": ["help@institute.org", "webmaster@institute.org"],
  "icon-link": "http://institute.org/images/other-avatar.png"
}
```

##### Response
Status Code on success: ```200 OK```  

---
#### delete user
Required access level: 1  
##### Request
Method: ```DELETE```  
URL: ```ip:port/user/{username}```  
Example: ```ip:port/user/admin```  

##### Response
Status Code on success: ```200 OK```  

---
#### reset user password
Required access level: 1  
##### Request
Method: ```POST```  
URL: ```ip:port/user/{username}/reset```  
Example: ```ip:port/user/admin/reset```  
Body (example):  
```json
{
  "password": "verysavepassword"
}
```  

##### Response
Status Code on success: ```200 OK```  

---
#### disable user
Required access level: 8  
##### Request
Method: ```PUT```  
URL: ```ip:port/user/{username}/disable```  
Example: ```ip:port/user/admin/disable```  

##### Response
Status Code on success: ```200 OK```  

---
#### enable user
Required access level: 8  
##### Request
Method: ```PUT```  
URL: ```ip:port/user/{username}/enable```  
Example: ```ip:port/user/admin/enable```  

##### Response
Status Code on success: ```200 OK```  

---
#### request a new access token for the user
Required access level: 1  
The category can be omitted. If not category is specified the token will be usable on every category.
##### Request
Method: ```POST```  
URL: ```ip:port/user/{username}/token```  
Example: ```ip:port/user/admin/token```  
Body (example):  
```json
{
  "category": "tickets"
}
``` 

##### Response
Status Code on success: ```200 OK```  
Body (example):  
```json
{
  "Token":"cDsFdD9lkwc+0NEj6Jiv+KyKo50ApHFKyJ56Vd6LW0I=",
  "UserID":3,
  "AccessLevel":8,
  "Category":"tickets"
}
```

---
#### get access tokens of the user
Required access level: 1  
##### Request
Method: ```GET```  
URL: ```ip:port/user/{username}/token```  
Example: ```ip:port/user/admin/token```  

##### Response
Status Code on success: ```200 OK```  
Body (example):  
```json
[{
  "Token":"n4tVoHF+WpRGc27/qfBL4/taL7pI9J3OsxDOXVp9EAU=",
  "UserID":3,
  "AccessLevel":8,
  "Category":""
}, {
  "Token":"cDsFdD9lkwc+0NEj6Jiv+KyKo50ApHFKyJ56Vd6LW0I=",
  "UserID":3,
  "AccessLevel":8,
  "Category":"tickets"
}]
```

---
#### delete access token of the user
Required access level: 1  
##### Request
Method: ```DELETE```  
URL: ```ip:port/user/{username}/token```  
Example: ```ip:port/user/admin/token```  
Body (example):  
```json
{
  "access-token": "n4tVoHF+WpRGc27/qfBL4/taL7pI9J3OsxDOXVp9EAU="
}
```

##### Response
Status Code on success: ```200 OK```  

---
#### update a access token of the user
Required access level: 1  
Only the category can be omitted. If not specified the token is usable for every category.
##### Request
Method: ```PUT```  
URL: ```ip:port/user/{username}/token```  
Example: ```ip:port/user/admin/token```  
Body (example):  
```json
{
  "access-token": "cDsFdD9lkwc+0NEj6Jiv+KyKo50ApHFKyJ56Vd6LW0I=",
  "access-level": "3",
  "category": "users"
}
```

##### Response
Status Code on success: ```200 OK```  

---

### content management

---
#### get server / service status information
Required access level: none  
##### Request
Method: ```GET```  
URL: ```ip:port/api```  

##### Response
Status Code on success: ```200 OK```  
Body (example):  
```json
{
  "host": "127.0.0.1",
  "port": "8080",
  "status": "online",
  "uptime": 41308906724
}
```

---
#### get category specific stats
Required access level: 2  
##### Request
Method: ```GET```  
URL: ```ip:port/api/{category}/stats```  
Example: ```ip:port/user/inventory/stats```  

##### Response
Status Code on success: ```200 OK```  
Body (example): 
```json
{
    "count":5,
    "dataSize":757,
    "estimate":false,
    "millis":0,
    "ok":true
}
```

---
#### get entities of category
Required access level: 3  
##### Request
Method: ```POST```  
URL: ```ip:port/api/{category}```  
Example: ```ip:port/user/inventory```  
Body (example):  
```json
{
  "find": {"type": "cable"},
  "count": 4
}
```  

##### Response
Status Code on success: ```200 OK```  
Body (example):  
```json
[{
  "_id": "5c1bec177740e42501f98fd1",
  "_owner": 1,
  "type": "cable",
  "cost": 10
}, {
  "_id": "5c1ce68e7740e42312a64f62",
  "_owner": 1,
  "type": "cable",
  "cost": 25
}, {
  "_id": "5c1bec177740e42505f53ec8",
  "_owner": 2,
  "type": "cable",
  "cost": 10
}, {
   "_id": "5c1bec177740e42505f43ed7",
   "_owner": 1,
   "type": "cable",
   "cost": 15
 }]
```

---
#### get entity by id
Required access level: 1  
##### Request
Method: ```GET```  
URL: ```ip:port/api/id/{id}```  
Example: ```ip:port/id/US1```  

##### Response
Status Code on success: ```200 OK```  
Body (example): 
```json
[{
  "_id": "5c1ce68e7740e42312a64f62",
  "_owner": 1,
  "access-level": 8,
  "icon-link": "http://institute.org/images/avatar.png",
  "name": "Admin Dashboard Admin",
  "public-emails": ["help@institute.org"],
  "username": "admin"
}]
```

---
#### add new entities
Required access level: 4  
When adding a new entity it has to follow the schema defined in the `schemas` directory. The `_owner` and `_id` get set automatically.
Multiple entities can get added at the same time. But single entities still have to be enclosed by square brackets.  
##### Request
Method: ```POST```  
URL: ```ip:port/api/{category}```  
Example: ```ip:port/user/inventory``` 
Body (examples):  
```json
{
  "data": [{
    "type": "cable",
    "cost": 12.5
  }]
}
``` 

##### Response
Status Code on success: ```200 OK```  

---
#### add entity n times
Required access level: 4  
When adding a new entity it has to follow the schema defined in the `schemas` directory. The `_owner` and `_id` get set automatically.
`count` has to be a integer larger than zero. No square brackets this time.
##### Request
Method: ```POST```  
URL: ```ip:port/api/{category}/{count}```  
Example: ```ip:port/user/inventory/10``` 
Body (examples):  
```json
{
  "data": [{
    "type": "cable",
    "cost": 12.5
  }]
}
``` 

##### Response
Status Code on success: ```200 OK```  

---
#### update a existing entity
Required access level: 5  
When updating a entity the whole entity gets replaced. The `_owner` and `_id` stay the same. Only the first entity matching the key value pattern gets updated.
##### Request
Method: ```PUT```  
URL: ```ip:port/api/{category}```  
Example: ```ip:port/user/inventory```  
Body (examples):  
```json
{
  "data": [{
    "type": "cable",
    "cost": 9
  }],
  "find": {"type": "cable", "cost":  12.5}
}
``` 

##### Response
Status Code on success: ```200 OK```  

---
#### delete existing entities
Required access level: 6  
Delete removes all matching entities. Chose your key value pair wisely.
##### Request
Method: ```DELETE```  
URL: ```ip:port/api/{category}/{key}/{value}```  
Example: ```ip:port/user/inventory/type/cable```  

##### Response
Status Code on success: ```200 OK```  

---

