package database

import (
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"time"
)

// variables to store the database access and stmt objects for later sql requests
var (
	db 					*sql.DB
	stmtAddUser 		*sql.Stmt
	stmtUpdUser 		*sql.Stmt
	stmtGetUser 		*sql.Stmt
	stmtGetUserID 		*sql.Stmt
	stmtDelUser 		*sql.Stmt
	stmtAddSession 		*sql.Stmt
	stmtUpdSession		*sql.Stmt
	stmtGetSession 		*sql.Stmt
	stmtGetSessions		*sql.Stmt
	stmtDelSession 		*sql.Stmt
	stmtAddToken 		*sql.Stmt
	stmtUpdToken		*sql.Stmt
	stmtGetToken 		*sql.Stmt
	stmtGetTokenID 		*sql.Stmt
	stmtDelToken 		*sql.Stmt
	// stmt to store log access
	stmtAddLog 			*sql.Stmt
	stmtGetLog 			*sql.Stmt
	stmtGetLogType		*sql.Stmt
	stmtGetLogHost		*sql.Stmt
	// store the time it takes for a session id to become invalid
	sessionExpiration time.Duration
)

// type to hold a authentication user object from the database
type User struct {
	ID 				int
	Username 		string
	PasswordHash	[]byte
	PasswordSalt	string
	IsDisabled		bool
	AccessLevel		int
	PrivateEmail	string
}

// type to hold a session id
type UserSession struct {
	SessionKey 		[]byte
	UserID 			int
	LoginTime		time.Time
	LastSeenTime	time.Time
	AccessLevel		int
}

// type to hold a access token
type AccessToken struct {
	Token 			string
	UserID			int
	AccessLevel		int
	Category		string
}

// type to hold a log entry
type LogEntry struct {
	Time 		int 	`json:"time"`
	Severity 	int		`json:"severity"`
	LogType 	string	`json:"log-type"`
	IP 			string	`json:"ip"`
	Host 		string	`json:"host"`
	Extra 		string	`json:"extra"`
}

// initialize connection to authdb (sql) and setup stmt objects with sql statements
func InitAuthDB(expiration time.Duration) {
	// create connection to sql database
	rdb, err := sql.Open("mysql", "adapi:ipada@/adapi?parseTime=true")
	if err != nil {
		panic(err)
	}
	db = rdb
	// create request prototypes
	stmtAddUser, err = 		db.Prepare("INSERT INTO User (Username, PasswordHash, PasswordSalt, IsDisabled, AccessLevel, PrivateEmail) VALUES (?, ?, ?, ?, ?, ?);")
	stmtGetUser, err = 		db.Prepare("SELECT * FROM User WHERE Username = ?;")
	stmtUpdUser, err =		db.Prepare("UPDATE User SET Username=?, PasswordHash=?, PasswordSalt=?, IsDisabled=?, AccessLevel=?, PrivateEmail=? WHERE Username = ?;")
	stmtGetUserID, err =	db.Prepare("SELECT * FROM User WHERE ID = ?;")
	stmtDelUser, err = 		db.Prepare("DELETE FROM User WHERE Username = ?;")
	stmtAddSession, err = 	db.Prepare("INSERT INTO UserSession (SessionKey, UserID, LoginTime, LastSeenTime, AccessLevel) VALUES (?, ?, ?, ?, ?);")
	stmtUpdSession, err =	db.Prepare("UPDATE UserSession SET LoginTime=?, LastSeenTime=?, AccessLevel=? WHERE SessionKey=?;")
	stmtGetSession, err = 	db.Prepare("SELECT * FROM UserSession WHERE SessionKey = ?;")
	stmtGetSessions, err = 	db.Prepare("SELECT * FROM UserSession;")
	stmtDelSession, err = 	db.Prepare("DELETE FROM UserSession WHERE SessionKey = ?;")
	stmtAddToken, err = 	db.Prepare("INSERT INTO AccessToken (Token, UserID, AccessLevel, Category) VALUES (?, ?, ?, ?);")
	stmtUpdToken, err =		db.Prepare("UPDATE AccessToken SET AccessLevel=?, Category=? WHERE Token=?;")
	stmtGetToken, err = 	db.Prepare("SELECT * FROM AccessToken WHERE Token = ?;")
	stmtGetTokenID, err = 	db.Prepare("SELECT * FROM AccessToken WHERE UserID = ?;")
	stmtDelToken, err = 	db.Prepare("DELETE FROM AccessToken WHERE Token = ?;")
	// for log
	stmtAddLog, err = 		db.Prepare("INSERT INTO Log (Time, Severity, LogType, IP, Host, Extra) VALUES (?, ?, ?, ?, ?, ?);")
	stmtGetLog, err = 		db.Prepare("SELECT * FROM Log order by Time LIMIT ?,?;")
	stmtGetLogType, err = 	db.Prepare("SELECT * FROM Log WHERE LogType=? order by Time LIMIT ?,?;")
	stmtGetLogHost, err = 	db.Prepare("SELECT * FROM Log WHERE Host=? order by Time LIMIT ?,?;")
	// package specific variable to store the expiration time from the config file
	sessionExpiration = expiration
}

// close all stmt objects
// and the database
func CloseAuthDB() {
	stmtAddUser.Close()
	stmtGetUser.Close()
	stmtUpdUser.Close()
	stmtGetUserID.Close()
	stmtDelUser.Close()
	stmtAddSession.Close()
	stmtUpdSession.Close()
	stmtGetSession.Close()
	stmtGetSessions.Close()
	stmtDelSession.Close()
	stmtAddToken.Close()
	stmtUpdToken.Close()
	stmtGetToken.Close()
	stmtGetTokenID.Close()
	stmtDelToken.Close()
	db.Close()
}

// procedure code : 400
func GeneratePasswordHash(password, salt string) []byte {
	// create a password hash by hashing the password with salt
	key := sha256.Sum256([]byte(password + salt))
	// convert the array to a slice
	return key[:]
}

// procedure code : 401
func GeneratePasswordSalt() string {
	// generate password salt by encoding a hashed string to base64
	// the hashed string is a password hash were the password is the current time and the salt is SALTSALZT
	return base64.StdEncoding.EncodeToString(GeneratePasswordHash(time.Now().String(), "SALTSALT"))[:8]
}

// procedure code : 410
func AddUser(user *User) *utils.ErrorList {
	// call stmt object with parameters from user
	res, err := stmtAddUser.Exec(user.Username, user.PasswordHash, user.PasswordSalt, user.IsDisabled, user.AccessLevel, user.PrivateEmail)
	if err != nil {
		return utils.CreateErrorList(err, "error adding user to Database", 410004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 410014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 410024500)
	}
	return nil
}

// procedure code : 411
func UpdateUser(username string, user *User) *utils.ErrorList {
	// call stmt object with parameters from user and username
	res, err := stmtUpdUser.Exec(user.Username, user.PasswordHash, user.PasswordSalt, user.IsDisabled, user.AccessLevel, user.PrivateEmail, username)
	if err != nil {
		return utils.CreateErrorList(err, "error updating user in Database", 411004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 411014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 411024500)
	}
	return nil
}

// procedure code : 420
func GetUser(username string) (*User, *utils.ErrorList) {
	// create user object
	user := User{}
	// call stmt object with username and scan result for user attributes
	err := stmtGetUser.QueryRow(username).Scan(&user.ID, &user.Username, &user.PasswordHash, &user.PasswordSalt, &user.IsDisabled, &user.AccessLevel, &user.PrivateEmail)
	if err != nil {
		return nil, utils.CreateErrorList(err, "error getting user from Database", 420004500)
	}
	// return user
	return &user, nil
}

// procedure code : 421
func GetUserByID(id int) (*User, *utils.ErrorList) {
	// create user object
	user := User{}
	// call stmt object with username and scan result for user attributes
	err := stmtGetUserID.QueryRow(id).Scan(&user.ID, &user.Username, &user.PasswordHash, &user.PasswordSalt, &user.IsDisabled, &user.AccessLevel, &user.PrivateEmail)
	if err != nil {
		return nil, utils.CreateErrorList(err, "error getting user from Database", 421004500)
	}
	// return user
	return &user, nil
}

// procedure code : 412
func DeleteUser(username string) *utils.ErrorList {
	// call stmt object with username
	res, err := stmtDelUser.Exec(username)
	if err != nil {
		return utils.CreateErrorList(err, "error removing user from Database", 412004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 412014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 412024500)
	}
	return nil
}

// procedure code : 430
func GenerateSessionKey(user *User) []byte {
	// generate a session id by hashing the username and the current time
	key := sha256.Sum256([]byte(user.Username + time.Now().String()))
	// convert the byte array to a slice
	return key[:]
}

// procedure code : 440
func AddSession(session *UserSession) *utils.ErrorList {
	// call stmt object with parameters from session
	res, err := stmtAddSession.Exec(session.SessionKey, session.UserID, session.LoginTime, session.LastSeenTime, session.AccessLevel)
	if err != nil {
		return utils.CreateErrorList(err, "error adding Session to Database", 440004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 440014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 440024500)
	}
	return nil
}

// procedure code : 441
func UpdateSession(key []byte, session *UserSession) *utils.ErrorList {
	// call stmt object with parameters from session
	res, err := stmtUpdSession.Exec(session.LoginTime, session.LastSeenTime, session.AccessLevel, key)
	if err != nil {
		return utils.CreateErrorList(err, "error updating session in Database", 441004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 441014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 441024500)
	}
	return nil
}

// procedure code : 450
func GetSession(key []byte) (*UserSession, *utils.ErrorList) {
	// create session object
	session := UserSession{}
	// call stmt object with session id / key and scan result for session attributes
	err := stmtGetSession.QueryRow(key).Scan(&session.SessionKey, &session.UserID, &session.LoginTime, &session.LastSeenTime, &session.AccessLevel)
	if err != nil {
		return nil, utils.CreateErrorList(err, "error getting session from Database", 450004500)
	}
	return &session, nil
}

// procedure code : 451
func GetSessions() (*[]UserSession, *utils.ErrorList) {
	// get all sessions in database
	rows, err := stmtGetSessions.Query()
	if err != nil {
		return nil, utils.CreateErrorList(err, "error getting sessions from Database", 451004500)
	}
	// always close rows
	defer rows.Close()
	// session slice to store sessions from database
	var sessions []UserSession
	// iterate over all rows
	for rows.Next() {
		// temporary variable to store the session to
		var session UserSession
		// scan the row for session attributes
		if err := rows.Scan(&session.SessionKey, &session.UserID, &session.LoginTime, &session.LastSeenTime, &session.AccessLevel); err != nil {
			return nil, utils.CreateErrorList(err, "error getting sessions from Database", 451014500)
		}
		// if there is no error append session to slice
		sessions = append(sessions, session)
	}
	if err := rows.Err(); err != nil {
		return nil, utils.CreateErrorList(err, "error getting sessions from Database", 451024500)
	}
	// return session slice
	return &sessions, nil
}

// procedure code : 442
func DeleteSession(key []byte) *utils.ErrorList {
	// call stmt object with session id / key
	res, err := stmtDelSession.Exec(key)
	if err != nil {
		return utils.CreateErrorList(err, "error removing Session from Database", 442004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 442014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 442024500)
	}
	return nil
}


// procedure code : 431
func ValidateSessionKey(key []byte) (int, int, *utils.ErrorList) {
	// get session with key from database
	session, errs := GetSession(key)
	// if there is an error the session key is invalid
	if errs != nil {
		errs.Add(utils.Error(errors.New("access denied"), "session key is invalid", 431002401))
		return -1, -1, errs
	}
	// get user with the id specified in the session object
	user, errs := GetUserByID(session.UserID)
	// if there is an error the user is not authenticated
	if errs != nil {
		return -1, -1, errs
	}
	// check if user is disabled
	if user.IsDisabled {
		return -1, -1, utils.CreateErrorList(errors.New("access denied"), "user is disabled", 431012401)
	}
	// check if session id has expired
	if time.Since(session.LoginTime) > sessionExpiration {
		return -1, -1, utils.CreateErrorList(errors.New("access denied"), "key has expired", 431022401)
	}
	// if the session id is valid return the access level for the user id
	return session.AccessLevel, session.UserID, nil
}


// procedure code : 460
func GenerateAccessToken() string {
	// similar to generate session id but the result is encoded to base64
	// get hash by hashing the location string and the current time
	genKey := sha256.Sum256([]byte(time.Now().Location().String() + time.Now().String()))
	// encode the slice of the byte array to base 64
	return base64.StdEncoding.EncodeToString(genKey[:])
}

// procedure code : 470
func AddToken(token *AccessToken) *utils.ErrorList {
	// call stmt object with parameters from token
	res, err := stmtAddToken.Exec(token.Token, token.UserID, token.AccessLevel, token.Category)
	if err != nil {
		return utils.CreateErrorList(err, "error adding token to Database", 470004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 470014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 470024500)
	}
	return nil
}

// procedure code : 471
func UpdateToken(key []byte, token *AccessToken) *utils.ErrorList {
	// call stmt object with key and attributes from token as parameters
	res, err := stmtUpdToken.Exec(token.AccessLevel, token.Category, key)
	if err != nil {
		return utils.CreateErrorList(err, "error updating token in Database", 471004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 471014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 471024500)
	}
	return nil
}

// procedure code : 480
func GetToken(key []byte) (*AccessToken, *utils.ErrorList) {
	// create access token object
	token := AccessToken{}
	// call stmt object with key and scan result for token attributes
	err := stmtGetToken.QueryRow(key).Scan(&token.Token, &token.UserID, &token.AccessLevel, &token.Category)
	if err != nil {
		return nil, utils.CreateErrorList(err, "error getting token from Database", 480004500)
	}
	return &token, nil
}

// procedure code : 481
func GetTokensByUserId(id int) (*[]AccessToken, *utils.ErrorList) {
	// get all tokens with the user id equal to id
	// call stmt object with id
	rows, err := stmtGetTokenID.Query(id)
	if err != nil {
		return nil, utils.CreateErrorList(err, "error getting tokens from Database", 481004500)
	}
	// always close rows
	defer rows.Close()
	// create slice of access tokens
	var tokens []AccessToken
	// iterate over all rows returned
	for rows.Next() {
		// create temporary token
		var token AccessToken
		if err := rows.Scan(&token.Token, &token.UserID, &token.AccessLevel, &token.Category); err != nil {
			return nil, utils.CreateErrorList(err, "error getting tokens from Database", 481014500)
		}
		// append token to tokens
		tokens = append(tokens, token)
	}
	if err := rows.Err(); err != nil {
		return nil, utils.CreateErrorList(err, "error getting tokens from Database", 481024500)
	}
	// return tokens
	return &tokens, nil
}

// procedure code : 472
func DeleteToken(key []byte) *utils.ErrorList {
	// call stmt object with key
	res, err := stmtDelToken.Exec(key)
	if err != nil {
		return utils.CreateErrorList(err, "error removing token from Database", 472004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 472014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 472024500)
	}
	return nil
}

// procedure code : 461
func ValidateAccessToken(key []byte, category string) (int, int, *utils.ErrorList) {
	// get the token with the key equal to key
	token, errs := GetToken(key)
	// if there is an error the access token is invalid
	if errs != nil {
		errs.Add(utils.Error(errors.New("access denied"), "access token is invalid", 461002401))
		return -1, -1, errs
	}
	// if the category does not match the passed (requested) category and the category is not empty the token is invalid
	if category != token.Category && token.Category != "" {
		return -1, -1, utils.CreateErrorList(errors.New("access denied"), "token is invalid for this category", 461012401)
	}
	// if there is a error while getting the user associated with the token the token is invalid
	user, errs := GetUserByID(token.UserID)
	if errs != nil {
		return -1, -1, errs
	}
	// if the user is disabled the token is invalid
	if user.IsDisabled {
		return -1, -1, utils.CreateErrorList(errors.New("access denied"), "user is disabled", 461022401)
	}
	// if no errors occurred the token is valid
	// return the access level and the user id
	return token.AccessLevel, token.UserID, nil
}


// procedure code : 470
func DB() *sql.DB {
	// return a pointer to the database for use outside of the package
	return db
}

func AddLog(entry LogEntry) *utils.ErrorList {
	// call stmt object with parameters from user
	res, err := stmtAddLog.Exec(entry.Time, entry.Severity, entry.LogType, entry.IP, entry.Host, entry.Extra)
	if err != nil {
		return utils.CreateErrorList(err, "error adding user to Database", 410004500)
	}
	rowCount, err := res.RowsAffected()
	if err != nil {
		return utils.CreateErrorList(err, "error getting count of affected rows", 410014500)
	}
	if rowCount != 1 {
		return utils.CreateErrorList(err, "error zero or more than one row affected", 410024500)
	}
	return nil
}

func GetLog(logType, host string, from, count int) (*[]LogEntry, *utils.ErrorList) {
	var rows *sql.Rows
	var err error
	if host == "" {
		if logType == "" {
			rows, err = stmtGetLog.Query(from, from + count)
		} else {
			rows, err = stmtGetLogType.Query(logType, from, from + count)
		}
	} else {
		rows, err = stmtGetLogHost.Query(host, from, from + count)
	}

	if err != nil {
		return nil, utils.CreateErrorList(err, "error getting entries from Database", 481004500)
	}
	// always close rows
	defer rows.Close()
	// create slice of log entries
	var entries []LogEntry
	// iterate over all rows returned
	for rows.Next() {
		// create temporary log entry
		var entry LogEntry
		var id int
		if err := rows.Scan(&id, &entry.Time, &entry.Severity, &entry.LogType, &entry.IP, &entry.Host, &entry.Extra); err != nil {
			return nil, utils.CreateErrorList(err, "error getting entries from Database", 481014500)
		}
		// append entry to entries
		entries = append(entries, entry)
	}
	if err := rows.Err(); err != nil {
		return nil, utils.CreateErrorList(err, "error getting entries from Database", 481024500)
	}
	return &entries, nil
}