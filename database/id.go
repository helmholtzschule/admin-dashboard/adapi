package database

import (
	"errors"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"gopkg.in/mgo.v2/bson"
	"log"
	"regexp"
	"strconv"
)

/* mongodb content:
 * {"_id":"US1402", "id":"a8f5d5e2a18d"}
 *	 _id: adid (Admin Dashboard ID)
 *   id:  id for relation with entities
 */

// procedure code : 300
func GetIdFromADID(adid string) (bson.ObjectId, *utils.ErrorList) {
	// get adid object with the specified id from the database
	idObj, errs := GetFromNEntities("adid", bson.M{"_id": adid}, 0, 1)
	if errs != nil {
		return bson.ObjectId(0), errs
	}
	if *idObj == nil {
		return bson.ObjectId(0), utils.CreateErrorList(errors.New("not found"), "can't find a object with this adid", 300006400)
	}
	// return the id of the first and only object
	return (*idObj)[0]["id"].(bson.ObjectId), nil
}

// procedure code : 310
func GetNewADID(category string) (string, *utils.ErrorList) {
	// check if the category name is valid
	if cat, ok := CATEGORIES[category]; ok {
		// get collection adid from content database
		c := session.DB("adapi").C("adid")
		// get the number of adids of the same category
		var count, err = c.Find(bson.M{"_id": bson.M{"$regex": cat}}).Count()
		if err != nil {
			log.Fatal(err)
		}
		// return the short version of the category and the next id as a string
		// adids start at 0
		return cat + strconv.Itoa(count), nil
	}
	return "", utils.CreateErrorList(errors.New("error getting id"), "could not get a new adid", 310003500)
}

// procedure code : 311
func RegisterADID(id bson.ObjectId, adid string) *utils.ErrorList {
	// get collection adid from content database
	c := session.DB("adapi").C("adid")
	// insert adid entity into database
	err := c.Insert(bson.M{"id":id, "_id":adid})
	if err != nil {
		return utils.CreateErrorList(err, "could not create new id", 311003500)
	}
	return nil
}

// procedure code : 301
func GetCategoryFromADID(adid string) (string, *utils.ErrorList) {
	// compile a regex that matches the short version of the category of a adid
	pattern, err := regexp.Compile("[A-Z]+")
	if err != nil {
		return "", utils.CreateErrorList(err, "could not create new id", 301003500)
	}
	// use the result of regex find as a key for the long version category name
	return CATEGORIES_REVERSE[string(pattern.Find([]byte(adid)))], nil
}

// procedure code : 320
/*func RemoveADID(adid string) *utils.ErrorList {
	c := session.DB("adapi").C("adid")
	err := c.Remove(bson.M{"adid": adid})
	if err != nil {
		return utils.CreateErrorList(err, "could not remove adid", 0)
	}
	return nil
}*/