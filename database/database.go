package database

import (
	"errors"
	"github.com/xeipuuv/gojsonschema"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)
// variable to store the mongodb session
var session *mgo.Session

// type to store entities from the content (mongodb) database
type Entity bson.M

// type to store the schema for a category
type Category bson.M

// a map to store all accessible categories and their short form
var CATEGORIES = map[string]string {
	"badges":		"BA",
	"boards":		"BO",
	"inventory":	"IN",
	"issues":		"IS",
	"log":			"LG",
	"milestones":	"MS",
	"rooms": 		"RO",
	"stats": 		"ST",
	"tags":			"TA",
	"tickets": 		"TI",
	"users":		"US",
}

// the map above but reversed
// it is used to get from the short form to the category name
var CATEGORIES_REVERSE = utils.SwapMap(CATEGORIES)

// initialize a connection with the content database
func InitContentDB() {
	var err error
	// connect to the content database
	session, err = mgo.Dial("localhost:27017")
	if err != nil {
		panic(err)
	}
	// set mode to session
	session.SetMode(mgo.Monotonic, true)
}

// function to close the connection to the content database
func CloseContentDB() {
	session.Close()
}

// procedure code : 200
func CategoryExists(category string) (bool , *utils.ErrorList){
	// get collection categories from content database
	c := session.DB("adapi").C("categories")
	// get the number of entries for the category name
	var count, err = c.Find(bson.M{"_type":category}).Count()
	if err != nil {
		return false, utils.CreateErrorList(err, "could not get data from database", 200004500)
	}
	// if there are no entries return an error
	if count < 1 {
		return false, utils.CreateErrorList(err, "category does not exist", 200016400)
	}
	// if there are multiple entries return an error
	if count > 1 {
		return false, utils.CreateErrorList(err, "there are " + string(count) + " schemas for: " + category + " To many!", 200024500)
	}
	return true, nil
}

// procedure code : 201
func GetCategory(category string) (*Category, *utils.ErrorList) {
	// get collection categories from content database
	c := session.DB("adapi").C("categories")
	// temporary variable to store the category
	var result Category
	// find the category with the _type being the passed category name
	err := c.Find(bson.M{"_type":category}).Select(bson.M{"_id":0,"_type":0}).One(&result)
	if err != nil {
		return nil, utils.CreateErrorList(err, "could not get category from database", 201004500)
	}
	// return category
	return &result, nil
}

// procedure code : 202
func GetCategoryStats(category string) (*bson.M, *utils.ErrorList) {
	// get collection categories from content database
	// c := session.DB("adapi").C(category)
	var res struct{
		Estimate bool	`bson:"estimate"`
		NumObjects int	`bson:"numObjects"`
		Size int		`bson:"size"`
		Millis int		`bson:"millis"`
		Ok bool			`bson:"ok"`
	}
	// get stats for category with command
	err := session.DB("adapi").Run(bson.M{ "dataSize": "adapi." + category}, &res)
	if err != nil {
		return nil, utils.CreateErrorList(err, "could not get stats for category from database", 202004500)
	}
	// repack into bson
	result := bson.M{"count": res.NumObjects, "dataSize": res.Size, "estimate": res.Estimate, "millis": res.Millis, "ok": res.Ok}
	// return category
	return &result, nil
}

// procedure code : 210
func GetEntities(category string, m bson.M) (*[]Entity, *utils.ErrorList) {
	// get collection with the name specified in category from content database
	c := session.DB("adapi").C(category)
	// slice to store result
	var results []Entity
	// find all entities matching the bson object m
	err := c.Find(m).All(&results)
	if err != nil {
		return nil, utils.CreateErrorList(err, "could not get entities from database", 210004500)
	}
	// return entities
	return &results, nil
}

// procedure code : 211
func GetFromNEntities(category string, m bson.M, from, count int) (*[]Entity, *utils.ErrorList) {
	// get collection with the name specified in categories from content database
	c := session.DB("adapi").C(category)
	// slice to store result
	var results []Entity
	// get all entities matching the bson object m
	// but in the beginning from entities are skipped and the result is limited to count entities
	err := c.Find(m).Sort("_id").Skip(from).Limit(count).All(&results)
	if err != nil {
		return nil, utils.CreateErrorList(err, "could not get entities from database", 211004500)
	}
	// return entities
	return &results, nil
}

// procedure code : 212
func GetFromNEntitiesSorted(category string, m bson.M, from, count int, sort ...string) (*[]Entity, *utils.ErrorList) {
	// get collection with the name specified in categories from content database
	c := session.DB("adapi").C(category)
	// slice to store result
	var results []Entity
	// get all entities matching the bson object m
	// but in the beginning from entities are skipped and the result is limited to count entities
	// also the result is sorted by the keys defined in sort
	err := c.Find(m).Sort(sort...).Skip(from).Limit(count).All(&results)
	if err != nil {
		return nil, utils.CreateErrorList(err, "could not get entities from database", 212004500)
	}
	// return entities
	return &results, nil
}

// procedure code : 220
func AddEntity(category string, entity Entity, owner int) *utils.ErrorList {
	// validate entity first
	if errs := ValidateEntity(category, &entity); errs != nil {
		// if there are errors report them back to the user
		return errs
	}
	// generate a new object id
	id := bson.NewObjectId()
	// set the id to the newly created object id
	entity["_id"] = id
	// set the owner of the entity to the user creating the entity
	entity["_owner"] = owner
	// create new adid from category
	adid, errs := GetNewADID(category)
	if errs != nil {
		errs.Add(utils.Error(errors.New("error getting id"), "could not get a new adid", 220003500))
		return errs
	}
	// add adid to entity
	entity["_adid"] = adid
	// get collection with the name specified in categories from content database
	c := session.DB("adapi").C(category)
	// insert the entity to the collection
	err := c.Insert(entity)
	if err != nil {
		return utils.CreateErrorList(err, "could not insert entities into database", 220014500)
	}
	// register adid / put adid into database
	errs = RegisterADID(id, adid)
	// if there is an error try to remove the entity again and return the error
	if errs != nil {
		err := c.Remove(bson.M{"_id":id})
		// if there is an error while removing the entity again report the error back to the user
		if err != nil {
			errs.Add(utils.Error(errors.New("could not remove"), "error while removing entry after failed adid request", 220023500))
		}
		return errs
	}
	return nil
}

//TODO:DONE! check whether "_id" and "_owner" remain after update
// procedure code : 230
func UpdateEntity(category string, m bson.M, entity Entity) *utils.ErrorList {
	// get the entity from the database
	oldEntity, errs := GetFromNEntities(category, m, 0, 1)
	if errs != nil {
		return errs
	}
	// set the owner and the id of the updated user to the id, adid and owner of the old one
	entity["_id"] = (*oldEntity)[0]["_id"]
	entity["_owner"] = (*oldEntity)[0]["_owner"]
	entity["_adid"] = (*oldEntity)[0]["_adid"]
	// validate the entity again
	if errs = ValidateEntity(category, &entity); errs != nil {
		return errs
	}
	// get collection with the name specified in category from content database
	c := session.DB("adapi").C(category)
	// update the entity selected with bson
	err := c.Update(m, entity)
	if err != nil {
		return utils.CreateErrorList(err, "could not update entity in database", 230004500)
	}
	return nil
}

// procedure code : 240
func DeleteEntity(category string, m bson.M) *utils.ErrorList {
	c := session.DB("adapi").C(category)
	err := c.Remove(m)
	if err != nil {
		return utils.CreateErrorList(err, "could not delete entity from database", 240004500)
	}
	return nil
}


// procedure code : 205
func ValidateEntity(category string, entity *Entity) *utils.ErrorList {
	// get the schema of the category category
	subSchema, errs := GetCategory(category)
	if errs != nil {
		return errs
	}
	// append it to the necessary part of a schema
	var schema = bson.M{
		"id": "https://gitlab.com/helmholtzschule/admin-dashboard/adapi/schemas/users.json",
		"$schema": "http://json-schema.org/draft-07/schema#",
		"description": "A representation of a valid user of adapi.",
		"type": "object",
		"properties": subSchema,
	}
	// lead the entity to validate and put it inside the data tag
	// this is necessary because if we want to specify the required fields in the root of the json it would need to be
	// written in the same level as the $schema reference. This is however not possible to store in mongodb as mongo
	// does not allow the $ character
	documentLoader := gojsonschema.NewGoLoader(bson.M{"data":&entity})
	// load the schema
	schemaLoader := gojsonschema.NewGoLoader(schema)
	// validate schema
	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	// this is just in case there is a parsing error
	if err != nil {
		return utils.CreateErrorList(err, "there is an error in the json schema", 205004400)
	}
	// if the entity is valid return no error
	if result.Valid() {
		return nil
	} else {
		// if it is not create a new error list and fill it with errors in the entity
		errs = &utils.ErrorList{}
		for _, element := range result.Errors() {
			errs.Add(utils.Error(errors.New(element.String()), element.Description(), 205016400))
		}
	}
	// return errors
	return errs
}

// return a pointer to the mongodb session for use outside of the package
func Session() *mgo.Session {
	return session
}


