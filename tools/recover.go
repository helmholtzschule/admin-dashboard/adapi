package tools

import (
	"github.com/kataras/golog"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"math/rand"
	"strconv"
)

func RunRecover() {
	// create a new logger
	// and make the log look good
	logger := golog.New()
	logger.SetPrefix("\x1b[32m[ADAPI]\x1b[0m\x1b[34m[SETUP]\x1b[0m")
	// make sure the user is aware of what he is doing
	logger.Infof("Resetting the root user.")
	logger.Infof("Are you sure you want to proceed? There is no turning back!")
	// to make sure the user wants to proceed he has to enter the random number
	randInt := rand.Intn(256)
	logger.Infof("Please enter %d to verify that you want to proceed.", randInt)

	// get the number
	input := utils.GetText("number: ")
	// if it is not equal exit the program
	if strconv.Itoa(randInt) != input {
		logger.Infof("Exiting!")
	} else {
		// if it is equal let the user enter the email of the new root user
		logger.Infof("Please enter the new email of the root user.")
		email := utils.GetText("email: ")

	enterPassword:
		// let the user enter the new password twice
		logger.Infof("Please enter your new password.")
		password := utils.GetTextHidden("password: ")
		logger.Infof("Please reenter your password.")
		passwordConf := utils.GetTextHidden("password: ")
		// if there not the same do it again
		if password != passwordConf {
			logger.Warnf("Passwords entered are not the same! Please retry.")
			goto enterPassword
		}
		// generate password salt an hash
		salt := database.GeneratePasswordSalt()
		hash := database.GeneratePasswordHash(password, salt)
		// get the root user from the authentication database by his id 0
		root, errs := database.GetUserByID(0)
		if errs == nil {
			// set password hash and salt and the new email
			root.PasswordHash = hash
			root.PasswordSalt = salt
			root.PrivateEmail = email
			// update the root user in the authentication database
			errs = database.UpdateUser("root", root)
			if errs != nil {
				logger.Errorf("An error while updating the root user.")
			}
		} else {
			logger.Errorf("An error while getting the root user from database.")
		}

		logger.Infof("Done")
	}
}
